package ru.will0376.emojicordpermissions;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import scala.actors.threadpool.Arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Mod.EventBusSubscriber
public class Events {
	@SubscribeEvent
	public static void event(ServerChatEvent event) {
		EntityPlayerMP player = event.getPlayer();
		List<String> allEmoji = getAllEmoji(event.getMessage());
		for (String s : allEmoji) {
			if (EmojiCordPermissions.config.usePex) {
				if (EmojiCordPermissions.config.pex.containsKey(s)) {
					if (!player.canUseCommand(0, EmojiCordPermissions.config.pex.get(s))) {
						event.setCanceled(true);
						player.sendMessage(new TextComponentString(TextFormatting.RED + "Нет прав на использование!"));
					}
				} else {
					event.setCanceled(true);
					player.sendMessage(new TextComponentString(TextFormatting.RED + "Нет прав на использование!"));
				}
			}
		}
		if (!event.isCanceled()) {
			if (EmojiCordPermissions.config.count != -1) {
				boolean cancel = allEmoji.size() > EmojiCordPermissions.config.count;
				event.setCanceled(cancel);
				if (cancel)
					player.sendMessage(new TextComponentString(TextFormatting.RED + "Слишком много эмоций в 1 сообщении!"));

			}
		}
	}

	public static List<String> getAllEmoji(String string) {
		String[] split = string.split(":(\\w*):");
		List<String> arrayList = new ArrayList<String>(Arrays.asList(split)).stream()
				.map(e -> e.replaceAll(":", ""))
				.collect(Collectors.toList());
		return arrayList;
	}
}
