package ru.will0376.emojicordpermissions;

import net.minecraftforge.common.config.Configuration;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Config {
    public Configuration configuration;

    int count;
    boolean usePex;
    Map<String, String> pex = new HashMap<>();

    public Config(File file) {
        configuration = new Configuration(file);
    }

    public void save() {
        configuration.save();
    }

    public void load() {
        configuration.load();
    }

    public Config launch() {
        load();
        setConfigs();
        save();
        return this;
    }

    private void setConfigs() {
        Arrays.asList(configuration.getStringList("PexList", "General", new String[]{}, "emoji:pex")).forEach(e -> {
            try {
                pex.put(e.split(":")[0], e.split(":")[1]);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        count = configuration.getInt("maxEmj", "General", -1, -1, Integer.MAX_VALUE, "-1 for disable");
        usePex = configuration.getBoolean("usePex", "General", false, "usePex");
    }

}
