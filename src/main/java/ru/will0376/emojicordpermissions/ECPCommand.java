package ru.will0376.emojicordpermissions;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public class ECPCommand extends CommandBase {
	@Override
	public String getName() {
		return "ecp";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "";
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		if (args.length == 1) {
			return getListOfStringsMatchingLastWord(args, Arrays.asList(Command.values()));
		}
		return super.getTabCompletions(server, sender, args, targetPos);
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length >= 1) {
			Config config = EmojiCordPermissions.config;
			if (sender.canUseCommand(4, "ecp.adminCommands")) {
				switch (Command.valueOf(args[0])) {
					case addPex:
						if (args.length < 3) {
							sender.sendMessage(new TextComponentString("addPex <emoji> <pex>"));
							return;
						}
						config.pex.put(args[1], args[2]);
						sender.sendMessage(new TextComponentString("Done! added " + args[1] + ":" + args[2]));
						config.save();
						break;

					case removePex:
						if (args.length < 2) {
							sender.sendMessage(new TextComponentString("removePex <emoji>"));
							return;
						}
						config.pex.remove(args[1]);
						sender.sendMessage(new TextComponentString("Done!"));
						config.save();
						break;

					case setCount:
						if (args.length < 2) {
							sender.sendMessage(new TextComponentString("setCount <count>"));
							return;
						}
						config.count = Integer.parseInt(args[1]);
						config.save();
						sender.sendMessage(new TextComponentString("Done!"));
						break;

					case toggleUsePex:
						config.usePex = !config.usePex;
						config.save();
						sender.sendMessage(new TextComponentString("Done! usePex: " + config.usePex));
						break;

					case reload:
						config.load();
						sender.sendMessage(new TextComponentString("Done!"));
						break;

					case save:
						config.save();
						sender.sendMessage(new TextComponentString("Done!"));
						break;
				}
			}
		}
	}

	public enum Command {
		addPex,
		removePex,
		setCount,
		reload,
		save,
		toggleUsePex
	}
}
