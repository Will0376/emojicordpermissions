package ru.will0376.emojicordpermissions;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.relauncher.FMLLaunchHandler;

@Mod(modid = EmojiCordPermissions.MOD_ID, name = EmojiCordPermissions.MOD_NAME, version = EmojiCordPermissions.VERSION, acceptableRemoteVersions = "*", serverSideOnly = true)
public class EmojiCordPermissions {

	public static final String MOD_ID = "emojicordpermissions";
	public static final String MOD_NAME = "EmojiCordPermissions";
	public static final String VERSION = "1.0";
	public static boolean debug = true;
	public static Config config;
	@Mod.Instance(MOD_ID)
	public static EmojiCordPermissions INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		if (debug) debug = FMLLaunchHandler.isDeobfuscatedEnvironment();
		config = new Config(event.getSuggestedConfigurationFile()).launch();
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
	}

	@Mod.EventHandler
	public void serverStarting(FMLServerStartingEvent event) {
		event.registerServerCommand(new ECPCommand());
	}
}